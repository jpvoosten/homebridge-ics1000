"use strict";
const ics1000 = require('./ICS1000.js');

var Service, Characteristic;

module.exports = function (homebridge)
  {
	Service = homebridge.hap.Service;
	Characteristic = homebridge.hap.Characteristic;
	homebridge.registerAccessory('homebridge-ics1000', 'ICS1000-OnOff',   OnOffAccessory);
	homebridge.registerAccessory('homebridge-ics1000', 'ICS1000-Dimmer', DimmerAccessory);
  }

function OnOffAccessory(log, config)
  {
	this.log = log;
    this.name = config['name'];
    this.room = config['room'];
    this.device = config['device'];
    this.type = config['type'] || 'lightbulb';
  }
  
function DimmerAccessory(log, config)
  {
	this.log = log;
    this.name = config['name'];
    this.room = config['room'];
    this.device = config['device'];
  }

OnOffAccessory.prototype =
  {
  identify: function (callback)
    {
		this.log('Identify requested!');
		callback();
    },

  getServices: function ()
    {
		var informationService = new Service.AccessoryInformation();
		informationService
		  .setCharacteristic(Characteristic.Manufacturer, 'KlikAanKlikUit')
		  .setCharacteristic(Characteristic.Model, 'ICS-1000');

		let service;
		if (this.type == 'switch')
			service = new Service.Lightbulb(this.name);
		else
			service = new Service.Switch(this.name);

		service
		.getCharacteristic(Characteristic.On)
		.on('set', this.setOnOff.bind(this));
		
		return [informationService, service];
    },
	
  setOnOff: function (value, callback)
    {
		const state = Boolean(value);
		if(state == true) {
			this.log('SendON: Room=' + this.room + ' Device=' + this.device + ' name=' + this.name);
			ics1000.sendOn(this.room, this.device, this.name, callback);
		}
		else {
			this.log('SendOFF: Room=' + this.room + ' Device=' + this.device + ' name=' + this.name);
			ics1000.sendOff(this.room, this.device, this.name, callback);
		}
    },
  };
  
  
DimmerAccessory.prototype =
  {
  identify: function (callback)
    {
		this.log('Identify requested!');
		callback();
    },

  getServices: function ()
    {
		var informationService = new Service.AccessoryInformation();
		informationService
		  .setCharacteristic(Characteristic.Manufacturer, 'KlikAanKlikUit')
		  .setCharacteristic(Characteristic.Model, 'ICS-1000');
		
		const lightbulbService = new Service.Lightbulb(this.name);
		lightbulbService
		.getCharacteristic(Characteristic.On)
		.on('set', this.setOnOff.bind(this));
		
		lightbulbService
		.getCharacteristic(Characteristic.Brightness)
		.on('set', this.setDim.bind(this));
		
		return [informationService, lightbulbService];
    },
	
  setOnOff: function (value, callback)
    {
		const state = Boolean(value);
		if(state == true) {
			this.log('SendON: Room=' + this.room + ' Device=' + this.device + ' Name=' + this.name);
			ics1000.sendOn(this.room, this.device, this.name, callback);
		}
		else {
			this.log('SendOFF: Room=' + this.room + ' Device=' + this.device + ' Name=' + this.name);
			ics1000.sendOff(this.room, this.device, this.name, callback);
		}
    },
	
  setDim: function (level, callback)
    {
		const percentage = parseInt(level);
		
		this.log('SendDim: Room=' + this.room + ' Device=' + this.device + ' Name=' + this.name + 'Level=' + percentage);
		
		if(percentage <= 0) {
			ics1000.sendOff(this.room, this.device, this.name, callback);
		}
		else if(percentage >= 100) {
			ics1000.sendOn(this.room, this.device, this.name, callback);
		}
		else {
			ics1000.sendDim(this.room, this.device, this.name, percentage, callback);
		}		
    }
  };
  
