"use strict";
const dgram = require('dgram');
const broadcastPort = 9760;
const broadcastAddress = '255.255.255.255';
const listeningAddress = '0.0.0.0';
const listeningPort = 9761;
var commandId = 0;

module.exports = {
	sendOn: function(roomId, deviceId, name, callback) {
		const value = 'F1';
		this.send(roomId, deviceId, value, name, callback);
	},
	
	sendOff: function(roomId, deviceId, name, callback) {
		const value = 'F0';
		this.send(roomId, deviceId, value, name, callback);
	},
	
	sendDim: function(roomId, deviceId, name, level, callback) {
		const x = parseInt((level / 100.0) * 32.0); // 32 = 100%
		const value = 'FdP' + x;
		this.send(roomId, deviceId, value, name, callback);
	},
	
	send: function(roomId, deviceId, value, name, callback)
	{
		const socket = dgram.createSocket('udp4');
		const doBind = (listeningPort, listeningAddress) => {socket.bind(listeningPort, listeningAddress, () => { socket.setBroadcast(true); });};
		
		socket.on('message', (msg, rinfo) => {
			//console.log('UDP message received: ' + msg + ' from ' + rinfo.address + ':' + rinfo.port);
			socket.close();
			callback();
		});
		socket.on('error', (err) => {
			 if (err.code == 'EADDRINUSE') {
				//console.log('Listening address (' + listeningAddress + ') currently in use. Retrying after 1 second...');
				setTimeout(() => { doBind(listeningPort, listeningAddress); }, 1000);
			  }
			  else {
				//console.log('UDP error received: ' + err);
				socket.close();
				callback();  
			  }
		});
		socket.on('listening', () => {
			commandId++;
			const message =  commandId + ',!R' + roomId + 'D' + deviceId + value + '|' + name + '|' + value + '\n';
			//console.log('UDP broadcast: ' + message);
			const buffer = new Buffer(message);
			socket.send(buffer, 0, buffer.length, broadcastPort, broadcastAddress, (err) => { });
						
			if(commandId >= 999){ 
				commandId = 0;
			}
		});
		
		doBind(listeningPort, listeningAddress);
	}
}
